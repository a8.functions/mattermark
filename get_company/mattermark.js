"use strict"
const rp = require('request-promise');

exports.getCompany = function (id, key) {

	const options = {
	    uri: `https://api.mattermark.com/companies/${id}`,
	    headers: {
	        'Authorization': `Bearer ${key}`
	    },
	    json: true // Automatically parses the JSON string in the response
	};
	 
	return rp(options)
	    .then(res => {
	        return res;
	    })
	    .catch(function (err) {
	        // API call failed...
	    });

}
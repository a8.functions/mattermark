const fdk=require('@autom8/fdk');
const a8=require('@autom8/js-a8-fdk')
const Mattermark = require('./mattermark')

fdk.handle(function(input){

  	return Mattermark.getCompany(input.id, input.apiKey)
  		.then(res => {
  			return {"company": res}
  		})
  		.catch(err => {
  			return {"error": err}
  		})

})

// a8 invoke mattermark.get_company '{"id":554,"apiKey":"13596b1415938ab645dc43535b22ab88939ececceafe2517efff27f219180131"}'
// a8 invoke mattermark.search_companies '{"name":"google","apiKey":"13596b1415938ab645dc43535b22ab88939ececceafe2517efff27f219180131"}'
//157545
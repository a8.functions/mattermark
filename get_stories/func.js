const fdk=require('@autom8/fdk');
const a8=require('@autom8/js-a8-fdk')
const getStories = require('./getStories.js')

const missingKey = 'apiKey is a required field'
const missingId = 'id is a required field'

fdk.handle(function(input){
  const apiKey = input.apiKey
  const companyId = input.id
  const errors = []

  if (!apiKey || apiKey.length === 0) {
    errors.push(missingKey)
  }
 
  if (!companyId || companyId.length === 0) {
    errors.push(missingId)
  }

  if (errors.length !== 0) {
    return {errors}
  } else {
    return getStories(companyId, apiKey).then((stories) => {
      return {stories}
    })
  }
})

fdk.slack(function(response){
  const responseType = "in_channel"
  const blocks = []

  if (!response || !response.stories || !Array.isArray(response.stories)) {
    responseType = "ephemeral"
    blocks.push({
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "Something's gone wrong"
      }
    })
  } else if (response.stories.length === 0) {
    blocks.push({
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "No stories found for company"
      }
    })
  } else {
    response.stories.forEach((story, index) => {
      if (index < 4) {
        const storyLink = `*<${story.url}|${story.title}>*\n`
        const sourceTitle = `${story.source_title}\n`
        const publishedOn = `${story.published_at}\n`
        const markdown = storyLink + sourceTitle + publishedOn

        blocks.push({
          "type": "divider"
        })

        blocks.push({
          "type": "section",
          "text": {
            "type": "mrkdwn",
            "text": markdown
          }
        })
      }
    })
  }

  return {
    "response_type": responseType,
    "blocks": blocks
  }
})
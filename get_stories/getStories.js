'use strict'

const Promise = require('bluebird')
const rp = require('request-promise')

const mattermarkAPI = 'https://api.mattermark.com/companies/'
const storiesPath =  '/stories'

const searchCompanies = (companyId, apiKey) => {
    const options = {
        uri: mattermarkAPI + companyId + storiesPath,
        headers: {
            'Authorization': apiKey
        }
    }

    return rp(options)
        .then((res) => {
            return JSON.parse(res)
        })
        .catch((err) => {
            return {'error': 'Failed'}
        })
}

module.exports = searchCompanies
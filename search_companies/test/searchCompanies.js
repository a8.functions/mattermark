'use strict'

const assert = require('assert');
const searchCompanies = require('../searchCompanies.js')

describe ('mattermark', () => {
    describe('searchCompanies', () => {
        it('should return', (done) => {
            searchCompanies('keen home', '1e663fbd8064d213f21ab51381f91149267e38d952adee9fd6a7a3320066f8f5')
                .then((res) => {
                    const responseJSON = JSON.parse(res)
                    assert(responseJSON.companies && responseJSON.companies.length === 1)
                    done()
                })
                .catch(done)
        })
    })
})
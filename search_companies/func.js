const fdk=require('@autom8/fdk');
const a8=require('@autom8/js-a8-fdk')
const searchCompanies = require('./searchCompanies.js')

const missingKey = 'apiKey is a required field'
const missingName = 'name is a required field'

fdk.handle(function(input){
  const apiKey = input.apiKey
  const companyName = input.name
  const errors = []

  if (!apiKey || apiKey.length === 0) {
    errors.push(missingKey)
  }
 
  if (!companyName || companyName.length === 0) {
    errors.push(missingName)
  }

  if (errors.length !== 0) {
    return {errors}
  } else {
    return searchCompanies(companyName, apiKey)
  }
})


fdk.slack(function(response){
  const responseType = "in_channel"
  const blocks = []

  if (!response || !response.companies) {
    responseType = "ephemeral"
    blocks.push({
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "Something's gone wrong"
      }
    })
  } else if (response.companies.lenght === 0) {
    blocks.push({
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "No companies found"
      }
    })
  } else {
    blocks.push(response.companies.map((company) => {
      return {
        "type": "section",
        "text": {
          "type": "mrkdwn",
          "text": `*${company.company_name}*\n<https://${company.domain}>`
        }
      }
    }))
  }

  return {
    "response_type": responseType,
    "blocks": blocks
  }
})